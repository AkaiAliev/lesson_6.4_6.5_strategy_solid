//
//  DependencyInversion.swift
//  lesson_6.4_home_work_Strategy_SOLID
//
//  Created by Akai on 21/3/23.
//

import Foundation

// MARK: - 1st example
protocol DrivingProtocol {
    func startDriving()
    func isDriving() -> Bool
    func stopDriving()
}

final class Toyota: DrivingProtocol {
    func startDriving() {
        print("Toyota is started")
    }
    
    func isDriving() -> Bool {
        return true
    }
    
    func stopDriving() {
        print("Toyota is stopped")
    }
}

final class Audi: DrivingProtocol {
    func startDriving() {
        print("Audi is started")
    }
    
    func isDriving() -> Bool {
        return true
    }
    
    func stopDriving() {
        print("Audi is stopped")
    }
}

final class FavouriteCar {
    private let car: DrivingProtocol
    
    init(car: DrivingProtocol) {
        self.car = car
    }
}

