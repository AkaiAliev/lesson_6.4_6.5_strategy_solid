//
//  InterfaceSegregation.swift
//  lesson_6.4_home_work_Strategy_SOLID
//
//  Created by Akai on 21/3/23.
//

import Foundation

// MARK: - 1st example

protocol FeedableProtocol {
    func eat()
}

protocol WorkableProtocol {
    func work()
}

final class Human: FeedableProtocol, WorkableProtocol {
    func eat() {
        print("eating")
    }

    func work() {
        print("working")
    }
}

final class Robot: WorkableProtocol {
    func work() {
        print("working")
    }
}


// MARK: - 2nd example

protocol SomCurrencyProtocol {
    func som()
}

protocol USDCurrencyProtocol {
    func usd()
}

protocol RUBCurrencyProtocol {
    func rub()
}

protocol EURCurrencyProtocol {
    func eur()
}

final class VisaSingleCurrencySom: SomCurrencyProtocol {
    func som() {
        print("Одновалютная карта VISA имеет счет только в национальной валюте СОМ")
    }
    
    
}

final class VisaMultiCurrency: SomCurrencyProtocol, USDCurrencyProtocol, RUBCurrencyProtocol {
    func som() {
        print("Мультивалютная карта VISA. Счет в национальной валюте СОМ")
    }
    
    func usd() {
        print("Мультивалютная карта VISA. Счет в долларах США")
    }
    
    func rub() {
        print("Мультивалютная карта VISA. Счет в Российских Рублях")
    }
}

final class MasterCardMultiCurrency: SomCurrencyProtocol, USDCurrencyProtocol, EURCurrencyProtocol {
    func som() {
        print("Мультивалютная карта MasterCard. Счет в национальной валюте СОМ")
    }
    
    func usd() {
        print("Мультивалютная карта MasterCard. Счет в долларах США")
    }
    
    func eur() {
        print("Мультивалютная карта MasterCard. Счет в Российских Рублях")
    }
}
