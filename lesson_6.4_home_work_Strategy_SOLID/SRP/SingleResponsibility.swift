//
//  SingleResponsibility.swift
//  lesson_6.4_home_work_Strategy_SOLID
//
//  Created by Akai on 21/3/23.
//

import Foundation

// MARK: - 1st example

protocol TurnOnProtocol {
    func turnOn()
}

protocol TurnOffProtocol {
    func turnOff()
}

final class Light: TurnOnProtocol, TurnOffProtocol {
    
    private enum State {
        case turnOn
        case turnOff
    }
    
    private var state: State = .turnOff
    
    func turnOn() {
        state = .turnOn
    }
    
    func turnOff() {
        state = .turnOff
    }
}

final class TurnOnTheLight {
    private var light: TurnOnProtocol
    
    init(light: TurnOnProtocol) {
        self.light = light
    }
    
    func execute() {
        light.turnOn()
        print("Свет включен")
    }
}

final class TurnOffTheLight {
    private var light: TurnOffProtocol
    
    init(light: TurnOffProtocol) {
        self.light = light
    }
    
    func execute() {
        light.turnOff()
        print("Свет выключен")
    }
}



// MARK: - 2nd example

protocol GasolineEngineProtocol {
    func gasEngine()
}

protocol DieselEngineProtocol {
    func disEngine()
}

final class BMW: GasolineEngineProtocol, DieselEngineProtocol {

    private enum EngineOptions {
        case gasoline
        case diesel
    }

    private var engine: EngineOptions = .diesel

    func gasEngine() {
        engine = .gasoline
    }

    func disEngine() {
        engine = .diesel
    }
}

final class FillUpGas {
    private var bmwX5M: GasolineEngineProtocol

    init(bmwX5M: GasolineEngineProtocol) {
        self.bmwX5M = bmwX5M
    }

    func fillUpFuel() {
        bmwX5M.gasEngine()
        print("Бензиновый двигатель.Желательно заливать 98-100 бензин")
    }
}

final class FillUpDis {
    private var bmwX5M: DieselEngineProtocol

    init(bmwX5M: DieselEngineProtocol) {
        self.bmwX5M = bmwX5M
    }

    func fillUpFuel() {
        bmwX5M.disEngine()
        print("Двигатель на дизеле. Заливать только дизель")
    }
}
