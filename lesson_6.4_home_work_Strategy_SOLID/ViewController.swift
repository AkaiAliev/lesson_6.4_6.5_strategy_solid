//
//  ViewController.swift
//  lesson_6.4_home_work_Strategy_SOLID
//
//  Created by Akai on 20/3/23.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

// MARK: - Single responsibility
// MARK: - 1st example
let light = Light()
let lightSwitchOn = TurnOnTheLight(light: light)
lightSwitchOn.execute()

let lightSwitchOff = TurnOffTheLight(light: light)
lightSwitchOff.execute()

// MARK: - 2nd example
let bmw = BMW()
let gasolineFuel = FillUpGas(bmwX5M: bmw)
bmw

let dieselFuel = FillUpDis(bmwX5M: bmw)
bmw


// MARK: - Open-Closed
// MARK: - 1st example
let paymentManager = PaymentManager()
paymentManager.makePayment(amount: .oneThousand, payment: CashPayment())

// MARK: - 2nd example
let dogsLanguage = DogsLanguage()
dogsLanguage.makeSound(sound: .akitaSound, dog: Akita())


// MARK: - Interface Segregation
// MARK: - 1st example
let human = Human()
human.eat()
human.work()

let robot = Robot()
robot.work()

// MARK: - 2nd example
let visaSingleCurrencySom = VisaSingleCurrencySom()
visaSingleCurrencySom.som()

let visaMultiCurrency = VisaMultiCurrency()
visaMultiCurrency.som()
visaMultiCurrency.usd()
visaMultiCurrency.rub()

let masterCardMultiCurrency = MasterCardMultiCurrency()
masterCardMultiCurrency.som()
masterCardMultiCurrency.usd()
masterCardMultiCurrency.eur()

// MARK: - Dependency Inversion

// MARK: - 1st example
let toyota = FavouriteCar(car: Toyota())
toyota.car.startDriving()
toyota.car.isDriving()
toyota.car.stopDriving()

let audi = FavouriteCar(car: Audi())
audi.car.startDriving()
audi.car.isDriving()
audi.car.stopDriving()
