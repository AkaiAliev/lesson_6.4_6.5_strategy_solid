//
//  OpenClosed.swift
//  lesson_6.4_home_work_Strategy_SOLID
//
//  Created by Akai on 21/3/23.
//

import Foundation

// MARK: - 1st example

enum Amount: Double {
    case twenty = 20
    case fifty = 50
    case oneHundred = 100
    case twoHundred = 200
    case fiveHundred = 500
    case oneThousand = 1000
    case fiveThousand = 5000
}


protocol PaymentProtocol {
    func makePayment(amount: Amount)
}

final class CashPayment: PaymentProtocol {
    func makePayment(amount: Amount) {
        print("Оплата наличными")
    }
}

final class VisaPayment: PaymentProtocol {
    func makePayment(amount: Amount) {
        print("Оплата по карте VISA")
    }
}

final class MasterCardPayment: PaymentProtocol {
    func makePayment(amount: Amount) {
        print("Оплата по карте MasterCard")
    }
}

final class PaymentManager {
    func makePayment(amount: Amount, payment: PaymentProtocol) {
        payment.makePayment(amount: amount)
    }
}

// MARK: - 2nd example

enum DogsSound: String {
    case akitaSound = "Akita: Woof Woof Woof"
    case chowChowSound = "Chow Chow: woof"
    case dobermanSound = "Doberman: WOOF WOOF WOOF WOOF WOOF"
}

protocol DogsBreedProtocol {
    func makeSound(sound: DogsSound)
}

final class Akita: DogsBreedProtocol {
    func makeSound(sound: DogsSound) {
        print(sound)
    }
}

final class ChowChow: DogsBreedProtocol {
    func makeSound(sound: DogsSound) {
        print(sound)
    }
}

final class Doberman: DogsBreedProtocol {
    func makeSound(sound: DogsSound) {
        print(sound)
    }
}

final class DogsLanguage {
    func makeSound(sound: DogsSound, dog: DogsBreedProtocol) {
        dog.makeSound(sound: sound)
    }
}



