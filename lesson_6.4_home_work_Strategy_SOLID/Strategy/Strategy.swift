//
//  Strategy.swift
//  lesson_6.4_home_work_Strategy_SOLID
//
//  Created by Akai on 21/3/23.
//

import Foundation

// MARK: - 1st example

protocol SwimProtocol {
    func swim()
}

protocol DiveProtocol {
    func dive()
}

final class ProfessionalSwimmer: SwimProtocol {
    func swim() {
        print("professional swimming")
    }
}

final class NonSwimmer: SwimProtocol {
    func swim() {
        print("non-swimming")
    }
}

final class ProfessionalDiver: DiveProtocol {
    func dive() {
        print("professional diving")
    }
}

final class NonDiver: DiveProtocol {
    func dive() {
        print("non-diving")
    }
}

final class Human1 {
    private var swimBehaviour: SwimProtocol
    private var diveBehaviour: DiveProtocol
    
    init(swimBehaviour: SwimProtocol, diveBehaviour: DiveProtocol) {
        self.swimBehaviour = swimBehaviour
        self.diveBehaviour = diveBehaviour
    }
    
    func performSwim() {
        swimBehaviour.swim()
    }
    
    func setSwimBehaviour(sb: SwimProtocol) {
        self.swimBehaviour = sb
    }
    
    func performDive() {
        diveBehaviour.dive()
    }
    
    func setDiveBehaviour(db: DiveProtocol) {
        self.diveBehaviour = db
    }
}

let human1 = Human1(swimBehaviour: ProfessionalSwimmer(), diveBehaviour: ProfessionalDiver())
human1.performDive()
human1.performSwim()


// MARK: - 2nd example

protocol ServiceProvider {
    func connect()
}

final class WiFiService: ServiceProvider {
    func connect() {
        print("Connect through WiFi")
    }
}

final class ADSLService: ServiceProvider {
    func connect() {
        print("Connect through ADSL")
    }
}

class Customer {
    private let provider: ServiceProvider
    
    init(provider: ServiceProvider) {
        self.provider = provider
    }
    
    func accessInternet() {
        provider.connect()
    }
}

let firstCustomer = Customer(provider: WiFiService())
firstCustomer.accessInternet()

let secondCustomer = Customer(provider: ADSLService())
secondCustomer.accessInternet()
